<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterProductDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_detail', function (Blueprint $table) {
            $table->after('keyword', function ($table) {
                $table->integer('course_duration')->nullable();
                $table->text('course_description')->nullable();
                $table->text('course_program')->nullable();
                $table->text('course_faq')->nullable();
                $table->text('course_announcement')->nullable();
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
