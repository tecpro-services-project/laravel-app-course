<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product', function (Blueprint $table) {
            $table->after('price', function ($table) {
                $table->integer('student_amount')->nullable();
                $table->date('start_register_date')->nullable();
                $table->date('end_register_date')->nullable();
                $table->date('start_learn_date')->nullable();
                $table->date('end_learn_date')->nullable();
                $table->integer('instructor_id')->nullable();
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
