<?php

namespace Tecpro\Course\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Date;
use League\Csv\Reader;
use Tecpro\Ecommerce\Scripts\Managers\Facades\ProductMgr;

class CourseSeeder extends Seeder
{
    public function __construct()
    {
        // Date format must be: Y-m-d
        $this->now = Date::now();
    }

    /**
     * Fetch course data from spread sheet file
     * @return array Course data array
     */
    protected function fetchRecordFromOffice()
    {
        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load(base_path('import/course/course_import_data.xlsx'));
        $rows = $spreadsheet->getActiveSheet()->toArray();
        $header = null;
        $records = [];

        if (count($rows) > 1) {
            $header = $rows[0];
        }

        for ($idx = 1; $idx < count($rows); $idx++) {
            $row = $rows[$idx];
            $assoc = [];

            foreach ($header as $key => $keyName) {
                $cellValue = is_numeric($row[$key]) ? intval($row[$key]) : $row[$key];
                $assoc[$keyName] = $cellValue;
            }

            array_push($records, $assoc);
        }

        return $records;
    }

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $courses = $this->fetchRecordFromOffice();

        foreach ($courses as $course) {
            ProductMgr::create($course);
        }
    }
}
