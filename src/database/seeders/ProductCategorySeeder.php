<?php

namespace Tecpro\Course\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Str;
use Tecpro\Ecommerce\Scripts\Managers\Facades\ProductCategoryMgr;

class ProductCategorySeeder extends Seeder
{
    public function __construct() {
        $this->now = Date::now();
    }

    public function seedProductCategories() {
        $productCats = [
            [
                'id' => 'tester',
                'path' => 'tester/',
                'name' => 'Khóa học kiểm thử'
            ],
            [
                'id' => 'developer',
                'path' => 'developer/',
                'name' => 'Khóa học lập trình'
            ],
            [
                'id' => 'softskill',
                'path' => 'softskill/',
                'name' => 'Khóa học kĩ năng'
            ]
        ];

        foreach ($productCats as $cat) {
            $headLine = Str::headline($cat['id']);

            $cat['is_enable'] = 1;
            $cat['locale_id'] = 'vi';
            $cat['title'] = $cat['name'];
            $cat['description'] = $cat['name'];
            $cat['keyword'] = $headLine;
            $cat['created_at'] = $this->now;
            $cat['updated_at'] = $this->now;

            ProductCategoryMgr::create($cat);
        }
    }

    public function seedProductCategoryItems()
    {
        $productItems = [
            'fresher-tester-11-2022',
            'fresher-tester-12-2022',
            'oneone-tester-11-2022'
        ];

        foreach ($productItems as $item) {
            ProductCategoryMgr::createItem([
                'category_id' => 'tester',
                'product_id' => $item,
                'created_at' => $this->now,
                'updated_at' => $this->now
            ]);
        }
    }
    
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seedProductCategories();

        $this->seedProductCategoryItems();
    }
}
