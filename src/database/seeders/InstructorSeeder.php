<?php

namespace Tecpro\Course\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Date;
use Tecpro\Course\App\Models\Instructor;

class InstructorSeeder extends Seeder
{

    public function __construct() {
        $this->now = Date::now();
    }

    /**
     * Seed instructor data
     */
    protected function insertInstructors() {
        Instructor::insert([
            [
                'avatar' => null,
                'first_name' => 'Xuân',
                'last_name' => 'Võ',
                'email' => 'xuan.vo@tecproservices.net',
                'phone' => '999',
                'created_at' => $this->now,
                'updated_at' => $this->now,
            ],
            [
                'avatar' => null,
                'first_name' => 'Đạt',
                'last_name' => 'Kiều',
                'email' => 'dat.kieu@tecproservices.net',
                'phone' => '999',
                'created_at' => $this->now,
                'updated_at' => $this->now,
            ],
            [
                'avatar' => null,
                'first_name' => 'Tâm',
                'last_name' => 'Nguyễn',
                'email' => 'tam.nguyen@tecproservices.net',
                'phone' => '999',
                'created_at' => $this->now,
                'updated_at' => $this->now,
            ],
        ]);
    }

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->insertInstructors();
    }
}
