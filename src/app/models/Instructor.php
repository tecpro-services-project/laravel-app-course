<?php

namespace Tecpro\Course\App\Models;

use Tecpro\Core\App\Models\CoreModel;

class Instructor extends CoreModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'instructor';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'id',
        'avatar',
        'first_name',
        'last_name',
        'email',
        'phone'
    ];

    /**
     * Transform all necessary data into an associative array
     * @param string $localeId The locale ID
     * @return array
     */
    public function transform(string $localeId = '')
    {
        $final = $this->toArray();
        return array_merge($final, [
            'name' => $final['first_name'] . ' ' . $final['last_name'],
            'avatarUrl' => isset($final['avatar']) ? asset($final['avatar']) : ''
        ]);
    }
}
