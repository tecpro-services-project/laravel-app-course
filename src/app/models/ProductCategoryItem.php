<?php

namespace Tecpro\Course\App\Models;

use Tecpro\Ecommerce\App\Models\ProductCategoryItem as EcommerceProductCategoryItemModel;

class ProductCategoryItem extends EcommerceProductCategoryItemModel
{
    /**
     * Return product relation hasOne
     * @return \Illuminate\Database\Eloquent\Relations\HasOne Product relation hasOne
     */
    public function product()
    {
        return $this->hasOne(Product::class, 'id', 'product_id');
    }

    /**
     * Transform all necessary data into an associative array
     * @param string $localeId The locale ID
     * @return array
     */
    public function transform(string $localeId = '')
    {
        $final = $this->toArray();
        $product = $this->product()->get()->first();

        return array_merge($final, [
            'product' => isset($product) ? $product->transform($localeId) : null
        ]);
    }
}
