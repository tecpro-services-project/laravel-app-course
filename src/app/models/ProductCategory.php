<?php

namespace Tecpro\Course\App\Models;

use Tecpro\Ecommerce\App\Models\ProductCategory as EcommerceProductCategoryModel;

class ProductCategory extends EcommerceProductCategoryModel
{
    /**
     * Return category product items relation hasMany
     * @return \Illuminate\Database\Eloquent\Relations\HasMany Category product items relation hasMany
     */
    public function items()
    {
        return $this->hasMany(ProductCategoryItem::class, 'category_id', 'id');
    }
}
