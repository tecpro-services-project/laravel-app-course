<?php

namespace Tecpro\Course\App\Models;

use Tecpro\Ecommerce\App\Models\ProductDetail as EcommerceProductDetailModel;

class ProductDetail extends EcommerceProductDetailModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'id',
        'is_enable',
        'name',
        'title',
        'description',
        'keyword',
        'locale_id',
        'course_duration',
        'course_description',
        'course_program',
        'course_faq',
        'course_announcement',
    ];
}
