<?php

namespace Tecpro\Course\App\Models;

use Tecpro\Ecommerce\App\Models\Product as EcommerceProductModel;
use Tecpro\Ecommerce\Scripts\Objects\Price;

class Product extends EcommerceProductModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'id',
        'price',
        'student_amount',
        'start_register_date',
        'end_register_date',
        'start_learn_date',
        'end_learn_date',
        'instructor_id',
        'created_at',
        'updated_at'
    ];

    /**
     * Return product detail relation hasMany
     * @return \Illuminate\Database\Eloquent\Relations\HasMany Product detail relation hasMany
     */
    public function detail()
    {
        return $this->hasMany(ProductDetail::class, 'id', 'id');
    }

    /**
     * Get new product detail model
     * @return \Tecpro\Course\App\Models\ProductDetail The product detail model 
     */
    public function newDetail()
    {
        return new ProductDetail();
    }

    /**
     * Get the instructor for this course
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function instructor()
    {
        return $this->hasOne(Instructor::class, 'id', 'instructor_id');
    }

    /**
     * Transform all necessary data into an associative array
     * @param string $localeId The locale ID
     * @return array
     */
    public function transform(string $localeId = '')
    {
        $final = $this->toArray();
        $instructor = $this->instructor()->get()->first();

        return array_merge($final, [
            'detail' => $this->getDetailModel($localeId)->transform(),
            'price' => (new Price($final['price'], $localeId))->toArray(),
            'imageUrl' => isset($final['image']) ? asset($final['image']) : '',
            'instructor' => isset($instructor) ? $instructor->transform() : null,
        ]);
    }
}
